package com.example.sharkforecast.data;

import java.util.List;

public class ForecastResponse {
    private List<Forecast> list;

    public List<Forecast> getList() {
        return list;
    }
}
