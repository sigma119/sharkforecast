package com.example.sharkforecast.data;

public class ForecastGeneralData {
    private float temp;
    private float pressure;
    private float humidity;

    public ForecastGeneralData(float temp, float pressure, float humidity) {
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public float getTemp() {
        return temp;
    }

    public float getPressure() {
        return pressure;
    }

    public float getHumidity() {
        return humidity;
    }
}
