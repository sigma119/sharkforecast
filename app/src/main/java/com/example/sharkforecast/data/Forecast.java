package com.example.sharkforecast.data;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Forecast {
    @SerializedName("dt_txt")
    private String date;
    private ForecastGeneralData main;

    public ForecastGeneralData getForecastData() {
        return main;
    }

    public DateTime getDate() {
        DateTimeFormatter fm = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fm.parseDateTime(date);
    }
}
