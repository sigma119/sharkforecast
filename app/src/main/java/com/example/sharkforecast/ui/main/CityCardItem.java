package com.example.sharkforecast.ui.main;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sharkforecast.R;
import com.example.sharkforecast.ui.details.DetailsActivity;
import com.xwray.groupie.GroupieViewHolder;
import com.xwray.groupie.Item;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

/**
 * Card displayed in MainActivity.
 */
public class CityCardItem extends Item<GroupieViewHolder> {
    private final String cityName;
    private final int imageRes;

    /**
     * Takes city name which is passed to DetailsActivity.
     *
     * @param cityName - name of displayed city
     */
    CityCardItem(String cityName, @DrawableRes int imageRes) {
        this.cityName = cityName;
        this.imageRes = imageRes;
    }

    @Override
    public void bind(@NonNull GroupieViewHolder viewHolder, int position) {
        View v = viewHolder.itemView;
        ((ImageView) v.findViewById(R.id.backgroundImage)).setImageResource(imageRes);
        TextView title = v.findViewById(R.id.cityName);
        title.setText(cityName);
        v.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), DetailsActivity.class);
            i.putExtra(DetailsActivity.CITY_KEY, cityName);
            view.getContext().startActivity(i);
        });
    }

    @Override
    public int getLayout() {
        return R.layout.card_city;
    }
}
