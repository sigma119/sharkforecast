package com.example.sharkforecast.ui.main;

import android.os.Bundle;

import com.example.sharkforecast.R;
import com.xwray.groupie.GroupAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Starting activity.
 */
public class MainActivity extends AppCompatActivity {
    private GroupAdapter adapter = new GroupAdapter();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        TransitionManager.beginDelayedTransition(findViewById(R.id.rootView));
        adapter.add(new CityCardItem(getString(R.string.new_york), R.drawable.new_york));
        adapter.add(new CityCardItem(getString(R.string.washington), R.drawable.washington));
        adapter.add(new CityCardItem(getString(R.string.london), R.drawable.london));
    }
}
