package com.example.sharkforecast.ui.details;

import android.util.Log;

import com.example.sharkforecast.data.Forecast;
import com.example.sharkforecast.data.ForecastGeneralData;
import com.example.sharkforecast.data.ForecastResponse;
import com.example.sharkforecast.networking.ApiClient;
import com.example.sharkforecast.networking.WeatherService;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * DetailsActivity viewModel capable of fetching data for selected place
 */
public class DetailsViewModel extends ViewModel {

    private WeatherService service = ApiClient.getInstance().getWeatherService();
    private CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<List<ForecastGeneralData>> forecast = new MutableLiveData<List<ForecastGeneralData>>();

    /**
     * Initializes viewModel.
     *
     * @param cityName - city name to fetch data for.
     */
    void init(String cityName) {
        disposables.add(service.getForecastForCity(cityName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::mapForecastToDaily, err ->
                        Log.e("DetailsViewModel", "Error fetching data " + err.toString()))
        );
    }

    /**
     * Forecast returns data in 3hr period.
     * This maps it to daily by summing up the values and taking the medium from them.
     *
     * @param response - forecast response.
     */
    private void mapForecastToDaily(ForecastResponse response) {
        List<ForecastGeneralData> mappedData = new ArrayList<>();
        int forecastsPerDay = 0;
        float temperature = 0;
        float pressure = 0;
        float humidity = 0;
        int day = response.getList().get(0).getDate().getDayOfYear();

        for (Forecast data : response.getList()) {
            int dayOfYear = data.getDate().getDayOfYear();
            forecastsPerDay++;
            temperature += data.getForecastData().getTemp();
            pressure += data.getForecastData().getPressure();
            humidity += data.getForecastData().getHumidity();
            if (dayOfYear != day) {
                //reset counters
                mappedData.add(new ForecastGeneralData(
                        temperature / forecastsPerDay,
                        pressure / forecastsPerDay,
                        humidity / forecastsPerDay));
                forecastsPerDay = 0;
                temperature = 0;
                pressure = 0;
                humidity = 0;
                day++;
            }
        }
        //add last
        if (forecastsPerDay != 0) {
            mappedData.add(new ForecastGeneralData(
                    temperature / forecastsPerDay,
                    pressure / forecastsPerDay,
                    humidity / forecastsPerDay));
        }
        forecast.postValue(mappedData);
    }

    /**
     * Forecast mutable live data
     *
     * @return 5 day forecast live data
     */
    MutableLiveData<List<ForecastGeneralData>> getForecast() {
        return forecast;
    }
}
