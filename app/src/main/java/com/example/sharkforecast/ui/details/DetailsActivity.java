package com.example.sharkforecast.ui.details;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sharkforecast.R;
import com.example.sharkforecast.data.ForecastGeneralData;
import com.example.sharkforecast.utils.SharedPreferencesHelper;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.transition.TransitionManager;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity capable of displaying weather charts.
 */
public class DetailsActivity extends AppCompatActivity {
    public static final String CITY_KEY = "cityKey";
    public static final float CHART_GRANULARITY = 0.3f;
    public static final int API_CALL_LIMIT = 60;

    DetailsViewModel viewModel;
    @BindView(R.id.tempChart)
    LineChart tempChart;
    @BindView(R.id.pressureChart)
    LineChart pressureChart;
    @BindView(R.id.humidityChart)
    LineChart humidityChart;
    @BindView(R.id.humidityChartHolder)
    View humidityChartHolder;
    @BindView(R.id.pressureChartHolder)
    View pressureChartHolder;
    @BindView(R.id.tempChartHolder)
    View tempChartHolder;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.restrictionIcon)
    ImageView restrictionIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        viewModel = new ViewModelProvider(this).get(DetailsViewModel.class);
        prepareCharts();
        getWeatherData();
    }

    private void getWeatherData() {
        String requestedCity = Objects
                .requireNonNull(getIntent().getExtras()).getString(CITY_KEY, "");
        SharedPreferencesHelper prefs = new SharedPreferencesHelper(getApplicationContext());
        int callCount = prefs.getCallCount();
        if (callCount < API_CALL_LIMIT) {
            viewModel.init(requestedCity);
            prefs.incrementApiCallCount();
            Toast.makeText(this, "You can make " + (API_CALL_LIMIT - (callCount + 1)) + " calls more", Toast.LENGTH_SHORT).show();
        } else {
            TransitionManager.beginDelayedTransition(findViewById(R.id.rootView));
            restrictionIcon.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            Toast.makeText(this, "You have exceed the limits for an hour", Toast.LENGTH_SHORT).show();
        }
    }

    private void setChartsVisible() {
        TransitionManager.beginDelayedTransition(findViewById(R.id.rootView));
        tempChartHolder.setVisibility(View.VISIBLE);
        pressureChartHolder.setVisibility(View.VISIBLE);
        humidityChartHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void prepareCharts() {
        viewModel.getForecast().observe(this, data -> {
            List<Entry> tempEntries = new ArrayList<>();
            List<Entry> humidityEntries = new ArrayList<>();
            List<Entry> pressureEntries = new ArrayList<>();

            for (int i = 0; i < data.size(); i++) {
                ForecastGeneralData item = data.get(i);
                tempEntries.add(new Entry(i, item.getTemp()));
                humidityEntries.add(new Entry(i, item.getHumidity()));
                pressureEntries.add(new Entry(i, item.getPressure()));
            }

            LineDataSet tempSet = new LineDataSet(tempEntries, getString(R.string.temperature));
            LineDataSet humiditySet = new LineDataSet(humidityEntries, getString(R.string.humidity));
            LineDataSet pressureSet = new LineDataSet(pressureEntries, getString(R.string.pressure));

            ValueFormatter formatter = new ValueFormatter() {
                @Override
                public String getFormattedValue(float value) {
                    DateTime now = DateTime.now();
                    return getDayAsString(now.plusDays((int) value).getDayOfWeek());
                }
            };

            tempChart.setData(new LineData(tempSet));
            humidityChart.setData(new LineData(humiditySet));
            pressureChart.setData(new LineData(pressureSet));

            tempChart.getXAxis().setGranularity(CHART_GRANULARITY);
            tempChart.getXAxis().setGranularityEnabled(true);
            tempChart.getXAxis().setValueFormatter(formatter);
            tempChart.getXAxis().setAxisLineColor(R.color.colorPrimary);
            tempChart.getDescription().setEnabled(false);
            tempChart.invalidate();

            humidityChart.getXAxis().setGranularity(CHART_GRANULARITY);
            humidityChart.getXAxis().setGranularityEnabled(true);
            humidityChart.getXAxis().setValueFormatter(formatter);
            humidityChart.getXAxis().setAxisLineColor(R.color.colorPrimary);
            humidityChart.getDescription().setEnabled(false);
            humidityChart.invalidate();

            pressureChart.getXAxis().setGranularity(CHART_GRANULARITY);
            pressureChart.getXAxis().setGranularityEnabled(true);
            pressureChart.getXAxis().setValueFormatter(formatter);
            pressureChart.getXAxis().setAxisLineColor(R.color.colorPrimary);
            pressureChart.getDescription().setEnabled(false);
            pressureChart.invalidate();

            setChartsVisible();
        });
    }

    private String getDayAsString(int dayNumber) {
        switch (dayNumber) {
            case 1:
                return getString(R.string.monday);
            case 2:
                return getString(R.string.tuesday);
            case 3:
                return getString(R.string.thursday);
            case 4:
                return getString(R.string.wednesday);
            case 5:
                return getString(R.string.friday);
            case 6:
                return getString(R.string.saturday);
            case 7:
                return getString(R.string.sunday);
        }
        return "Unknown";
    }
}
