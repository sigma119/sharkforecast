package com.example.sharkforecast.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;


/**
 * Manages data stored in shared preferences.
 */
public class SharedPreferencesHelper {
    private static final String DATE_KEY = "dateKey";
    private static final String API_COUNTER_KEY = "apiCounterKey";
    private static final String SHARED_PREFERENCES = "prefs";
    private static final int HOURS_TO_BLOCK_FOR = 1;
    private SharedPreferences prefs;

    /**
     * SharedPreferencesHelper constructor.
     *
     * @param context - Application context
     */
    public SharedPreferencesHelper(Context context) {
        prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Returns current api call count and checks if counter should reset.
     *
     * @return api call count
     */
    public int getCallCount() {
        checkCounterResetTimer();
        return prefs.getInt(API_COUNTER_KEY, 0);
    }

    /**
     * Increments api call count saved in shared preferences.
     */
    public void incrementApiCallCount() {
        SharedPreferences.Editor e = prefs.edit();
        int counter = prefs.getInt(API_COUNTER_KEY, 0);
        counter++;
        e.putInt(API_COUNTER_KEY, counter).apply();
    }

    private void resetApiCallCount() {
        prefs.edit().putInt(API_COUNTER_KEY, 0).apply();
    }

    private void checkCounterResetTimer() {
        SharedPreferences.Editor e = prefs.edit();
        String date = prefs.getString(DATE_KEY, null);
        if (date == null) {
            date = DateTime.now().toString();
            e.putString(DATE_KEY, date).apply();
        }

        DateTime savedDate = DateTime.parse(date);

        if (savedDate.plusHours(HOURS_TO_BLOCK_FOR).isBeforeNow()) {
            e.putString(DATE_KEY, DateTime.now().toString()).apply();
            resetApiCallCount();
        }
    }
}
