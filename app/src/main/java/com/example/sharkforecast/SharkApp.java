package com.example.sharkforecast;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

public class SharkApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
