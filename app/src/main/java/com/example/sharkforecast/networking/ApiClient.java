package com.example.sharkforecast.networking;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton api client.
 */
public class ApiClient {
    private static ApiClient apiClient;
    private Retrofit client;

    public static ApiClient getInstance() {
        if (apiClient == null) {
            apiClient = new ApiClient();
            String BASE_URL = "https://api.openweathermap.org/data/2.5/";
            apiClient.client = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return apiClient;
    }

    public WeatherService getWeatherService() {
        return client.create(WeatherService.class);
    }
}
