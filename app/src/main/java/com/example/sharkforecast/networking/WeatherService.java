package com.example.sharkforecast.networking;

import com.example.sharkforecast.data.ForecastResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Service responsible for communicating with weather API.
 */
public interface WeatherService {
    @GET("forecast?appid=0fcd4b11132de53129d229f6659af20d")
    Single<ForecastResponse> getForecastForCity(@Query("q") String cityName);
}
